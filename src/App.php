<?php


namespace App;


class App
{

    /**
     * App constructor.
     */
    public function __construct()
    {
    }

    public function handle($int)
    {
        if ($int !== 0) {
            if ($int%3 === 0 && $int%5 === 0) {
                return "FizzBuzz";
            }

            if ($int%3 === 0) {
                return "Fizz";
            }

            if ($int%5 === 0) {
                return "Buzz";
            }
        }

        return $int;
    }
}