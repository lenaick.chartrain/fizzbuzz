<?php


use App\App;
use PHPUnit\Framework\TestCase;

class TestFizzBuzz extends TestCase
{
    public function testGiven0ThenIShouldHave0()
    {
        $app = new App();
        $result = $app->handle(0);
        $this->assertEquals(0, $result);
    }

    public function testGiven3WhenIHave3IShouldDisplayFizz()
    {
        $app = new App();
        $result = $app->handle(3);
        $this->assertEquals("Fizz", $result);

    }

    public function testGivenANumberWhenIHave3MultipleIShouldDisplayFizz()
    {
        $app = new App();
        $result = $app->handle(9);
        $this->assertEquals("Fizz", $result);

    }

    public function testGiven5WhenIHave5IShouldDisplayBuzz()
    {
        $app = new App();
        $result = $app->handle(5);
        $this->assertEquals("Buzz", $result);
    }

    public function testGivenANumberWhenIHave5MultipleIShouldDisplayBuzz()
    {
        $app = new App();
        $result = $app->handle(10);
        $this->assertEquals("Buzz", $result);

    }

    public function testGiven15WhenIHave3Or5MultipleIShouldDisplayFizzBuzz()
    {
        $app = new App();
        $result = $app->handle(15);
        $this->assertEquals("FizzBuzz", $result);
    }
}